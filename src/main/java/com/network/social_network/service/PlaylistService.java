package com.network.social_network.service;

import com.network.social_network.dto.playlist.PlaylistDto;
import com.network.social_network.dto.playlist.PlaylistResponseDto;
import com.network.social_network.dto.song.SongResponseDto;
import com.network.social_network.exception.CustomException;
import com.network.social_network.model.PlayListState;
import com.network.social_network.model.Playlist;
import com.network.social_network.model.Song;
import com.network.social_network.repository.PlaylistRepository;
import com.network.social_network.repository.SongRepository;
import com.network.social_network.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class PlaylistService {

    private final PlaylistRepository playlistRepository;
    private final UserRepository userRepository;
    private final FileUploadService fileUploadService;

    public PlaylistService(
            PlaylistRepository playlistRepository,
            UserRepository userRepository,
            FileUploadService fileUploadService
    ) {
        this.playlistRepository = playlistRepository;
        this.userRepository = userRepository;
        this.fileUploadService = fileUploadService;
    }

    public List<PlaylistResponseDto> getAll () {
        var playlistResponseDtoList = new ArrayList<PlaylistResponseDto>();

        for (var playlist : playlistRepository.findAll()) {
            var songs = new ArrayList<SongResponseDto>();
            for (Song s: playlist.getSongs()) {
                songs.add(new SongResponseDto(
                        s.getId(),
                        s.getUser().getUsername(),
                        s.getName(),
                        s.getGenre().getName(),
                        s.getSongFile().getFileName(),
                        s.getSongFile().getDuration(),
                        s.getLikes(),
                        s.getCover(),
                        s.getCreatedAt()
                ));
            }

            playlistResponseDtoList.add(new PlaylistResponseDto(
                    playlist.getId(),
                    playlist.getName(),
                    playlist.getPhoto(),
                    songs,
                    playlist.getUser().getUsername(),
                    playlist.getState().name()
            ));
        }
        return playlistResponseDtoList;
    }

    public PlaylistResponseDto getPlaylistById (Long playlistId) {
        var playlist = playlistRepository.findById(playlistId).orElseThrow(
                () -> new CustomException("Playlist with id " + playlistId + " not found", HttpStatus.NOT_FOUND)
        );

        var songs = new ArrayList<SongResponseDto>();
        for (Song s: playlist.getSongs()) {
            songs.add(new SongResponseDto(
                    s.getId(),
                    playlist.getUser().getUsername(),
                    s.getName(),
                    s.getGenre().getName(),
                    s.getSongFile().getFileName(),
                    s.getSongFile().getDuration(),
                    s.getLikes(),
                    s.getCover(),
                    s.getCreatedAt()
            ));
        }

        songs.sort(Collections.reverseOrder());

        var playlistDto = new PlaylistResponseDto(
                playlist.getId(),
                playlist.getName(),
                playlist.getPhoto(),
                songs,
                playlist.getUser().getUsername(),
                playlist.getState().name()
        );

        return playlistDto;
    }

    public void deletePlaylistById (Long playlistId) {
        var playlist = playlistRepository.findById(playlistId).orElseThrow(
                () -> new CustomException("Playlist with id " + playlistId + " not found", HttpStatus.NOT_FOUND)
        );
        playlist.setDeleted(true);
        playlistRepository.save(playlist);
    }

    public void createPlaylist (String username, PlaylistDto playlistDto) {

        var photoFile = fileUploadService.savePlaylistPhoto(playlistDto.getPhoto());

        //Todo: throw exception if user not found
        var playlist = new Playlist(
                userRepository.findByUsername(username),
                playlistDto.getName(),
                photoFile,
                playlistDto.getState() == 0 ? PlayListState.PRIVATE : PlayListState.PUBLIC,
                false);

        playlistRepository.save(playlist);
    }

    public void updatePlaylist (Long playlistId, PlaylistDto playlistDto) {
        var playlist = playlistRepository.findById(playlistId).orElseThrow(
                () -> new CustomException("Playlist with id " + playlistId + " not found", HttpStatus.NOT_FOUND)
        );

        playlist.setName(playlistDto.getName());
//        playlist.setPhoto(playlistDto.getPhoto());
        playlist.setState(playlistDto.getState() == 0 ? PlayListState.PRIVATE : PlayListState.PUBLIC);

        playlistRepository.save(playlist);
    }

    public List<PlaylistResponseDto> getPlaylistsByUsername (String username) {

        var user = userRepository.findByUsername(username);
        var playlists = new ArrayList<PlaylistResponseDto>();

        for (Playlist p: user.getPlaylists()) {

            playlists.add(new PlaylistResponseDto(
                 p.getId(),
                 p.getName(),
                 p.getPhoto(),
                 null,
                 username,
                 p.getState().name()
            ));
        }

        return playlists;
    }
}
